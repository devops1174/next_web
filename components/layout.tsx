import { FC, ReactNode } from "react";
import { Container, CssBaseline } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useSelector } from "react-redux";
import { RootState } from "../app/store";
import Login from "./login";

const theme = createTheme({
  palette: {
    mode: "light",
  },
});
interface IProps {
  children: ReactNode;
}

const Layout: FC<IProps> = ({ children }) => {
  const auth = useSelector((state: RootState) => state.auth);
  return (
    <ThemeProvider theme={theme}>
      <Container component="main">
        <CssBaseline />
        {auth.isAuth ? children : <Login></Login>}
      </Container>
    </ThemeProvider>
  );
};

export default Layout;
