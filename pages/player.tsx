import { NextPage } from "next";
import useSWR from "swr";

interface IData {
  success: boolean;
  message?: string;
}

const fetcher = async (url: string) => {
  const resp = await fetch(url);
  return await resp.json();
};

const PlayerPage: NextPage = () => {
  const { data, error } = useSWR<IData>(
    "http://localhost:3030/users/123445",
    fetcher
  );
  if (error) return <h1>Error</h1>;
  if (!data) return <h1>Loading.....</h1>;
  return (
    <>
      <h1>{data.message}</h1>
    </>
  );
};

export default PlayerPage;
