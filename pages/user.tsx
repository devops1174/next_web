import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import { NextPage } from "next";
import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  createUser,
  deleteUser,
  listUser,
  updateUser,
  UserListModel,
} from "../app/slices/user";
import { AppDispatch, RootState } from "../app/store";

const UserPage: NextPage = () => {
  const [mode, setMode] = useState<string>("ADD");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [id, setId] = useState(-1);

  const user = useSelector((state: RootState) => state.user);
  const dispatch = useDispatch<AppDispatch>();
  const [openDialog, setOpenDialog] = useState(false);
  const handleClose = () => {
    setOpenDialog(false);
  };
  const handleSave = () => {
    if (mode === "ADD") {
      dispatch(createUser({ email, name, password }))
        .then((_) => dispatch(listUser()))
        .then((_) => setOpenDialog(false));
    } else {
      dispatch(updateUser({ id, name, password }))
        .then((_) => dispatch(listUser()))
        .then((_) => setOpenDialog(false));
    }
  };
  const handleDel = (id: number) => {
    dispatch(deleteUser(id)).then(() => dispatch(listUser()));
  };

  const handleEdit = (user: UserListModel) => {
    setOpenDialog(true);
    setEmail(user.email);
    setName(user.name);
    setId(user.id);
    setMode("EDIT");
    setPassword(user.password);
  };

  const handleAdd = () => {
    setEmail("");
    setName("");
    setPassword("");
    setMode("ADD");
    setOpenDialog(true);
  };
  return (
    <>
      <Button onClick={() => dispatch(listUser())}>Search</Button>
      <Button onClick={() => handleAdd()}>New</Button>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Del</TableCell>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Edit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.users.map((row) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell>
                  <Button onClick={() => handleDel(row.id)}>Del</Button>
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell>
                  <Button onClick={(e) => handleEdit(row)}>Edit</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog open={openDialog} onClose={handleClose}>
        <DialogTitle>Subscribe</DialogTitle>
        <DialogContent>
          <DialogContentText>User Form</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            fullWidth
            variant="standard"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Password"
            type="password"
            fullWidth
            variant="standard"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSave}>Save</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default UserPage;
