import { NextPage } from "next";
import { useState } from "react";
import { io } from "socket.io-client";
const conn = io("https://localhost:8443", { transports: ["websocket"] });

const ChatPage: NextPage = () => {
  const [message, setMessage] = useState("");
  const [msgs, setMsgs] = useState<string[]>([]);

  conn.on("message", (e) => {
    setMsgs([...msgs, e]);
  });

  const sendHandler = () => {
    conn.emit("message", message);
  };

  const listMessage = () => {
    return msgs.map((v, idx) => <li key={idx}>{v}</li>);
  };

  return (
    <>
      <ul>{listMessage()}</ul>
      <input onChange={(e) => setMessage(e.target.value)}></input>
      <button onClick={sendHandler}>Send</button>
    </>
  );
};

export default ChatPage;
