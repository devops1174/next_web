import { GetServerSidePropsContext, NextPage } from "next";

interface IProps {
  success: boolean;
  message: string;
}

const ChatPage: NextPage<IProps> = ({ message, success }) => {
  return (
    <>
      <h1>{success ? message : "no data found !!!"}</h1>
    </>
  );
};

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  try {
    const { code } = context.params!;
    const resp = await fetch(`http://api:3030/users/${code}`);
    const body = await resp.json();
    return {
      props: body,
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        sucess: false,
        message: e,
      },
    };
  }
};

export default ChatPage;
