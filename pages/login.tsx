import { NextPage } from "next";
import {
  Box,
  Avatar,
  Typography,
  TextField,
  FormControlLabel,
  Button,
  Grid,
  Checkbox,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { FormEvent, useRef, useState } from "react";
import Link from "next/link";
import { signIn } from "next-auth/react";
import { AppDispatch, RootState } from "../app/store";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../app/slices/auth";
import { useRouter } from "next/router";

const LoginPage: NextPage = () => {
  const router = useRouter();
  const authState = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch<AppDispatch>();

  const userRef = useRef<HTMLInputElement>();
  const pwdRef = useRef<HTMLInputElement>();
  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    try {
      const body = {
        email: userRef.current?.value,
        password: pwdRef.current?.value,
      };
      const resp = await fetch("http://localhost:3030/login", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(body),
      });
      const res = await resp.json();
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  };

  const handleAuthSubmit = (e: any) => {
    signIn("credentials", {
      redirect: false,
      email: userRef?.current?.value,
      password: pwdRef?.current?.value,
      callbackUrl: `${window.location.origin}`,
    }).then((res) => {
      if (res?.ok) {
        console.log("login success");
        // router.push("/admin/dashboard");
      }
    });
  };

  const handleReduxAuth = (e: FormEvent<HTMLFormElement>) => {
    const email = userRef?.current?.value ?? "";
    const password = pwdRef?.current?.value ?? "";
    dispatch(login({ email, password })).then((res) => {
      router.push("/home");
    });
  };

  return (
    <>
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box
          component="form"
          onSubmit={(e) => {
            e.preventDefault();
            // handleSubmit(e);
            // handleAuthSubmit(e);
            handleReduxAuth(e);
          }}
          noValidate
          sx={{ mt: 1 }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            inputRef={userRef}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            inputRef={pwdRef}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="/player">Forgot password?</Link>
            </Grid>
            <Grid item>
              <Link href="/home">{"Don't have an account? Sign Up"}</Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </>
  );
};

export default LoginPage;
