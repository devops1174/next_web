import { NextApiRequest, NextApiResponse } from "next";

interface UserOut {
  status: boolean;
  data: UserModel[];
}

interface UserModel {
  code: string;
  name: string;
  age: number;
}

const handler = (req: NextApiRequest, res: NextApiResponse<UserOut>) => {
  res
    .status(200)
    .json({ status: true, data: [{ code: "", name: "1", age: "1" }] });
};

export default handler;
