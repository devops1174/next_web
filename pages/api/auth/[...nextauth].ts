import NextAuth, { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: { label: "Username", type: "text", placeholder: "jsmith" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const payload = {
          email: credentials?.email,
          password: credentials?.password,
        };
        const res = await fetch("http://api:3030/login", {
          method: "POST",
          body: JSON.stringify(payload),
          headers: { "Content-Type": "application/json" },
        });
        const user = await res.json();
        if (res.ok && user) {
          return { ...user };
        }
        // Return null if user data could not be retrieved
        return null;
      },
    }),
  ],
  //   callbacks: {},
  pages: {
    signIn: "/login",
  },
  secret: "super-secret-key",
  callbacks: {
    async jwt({ token, user, account }) {
      if (user && account) {
        return {
          ...token,
          accessToken: user?.token,
          refreshToken: user?.token,
        };
      }

      return token;
    },

    async session({ session, user, token }) {
      session.accessToken = token?.accessToken;

      return session;
    },
  },
};
export default NextAuth(authOptions);
