import { NextPage } from "next";
import { httpApi } from "../app/http-api";
import { wrapper } from "../app/store";

const HomePage: NextPage = () => {
  const testHandler = () => {
    httpApi
      .get("/user")
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
      });
  };
  return (
    <>
      <h1>My Home Page</h1>
      <button onClick={testHandler}>Test</button>
    </>
  );
};
export default HomePage;

export const getStaticProps = wrapper.getStaticProps<any>(
  (store) =>
    ({ preview }) => {
      console.log("2. Page.getStaticProps uses the store to dispatch things");

      store.dispatch({
        type: "TICK",
        payload: "was set in other page " + preview,
      });
      console.log(store.getState().counter.value);
      return {
        props: store.getState().counter.value,
      };
    }
);
