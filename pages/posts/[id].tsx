import { GetServerSidePropsContext, NextPage } from "next";
import Router, { useRouter } from "next/router";

const PostPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <>
      <h1>Post Page {id} </h1>
    </>
  );
};

export default PostPage;
