import { GetServerSidePropsContext, NextPage } from "next";

interface IProps {
  success: boolean;
  message: string;
}

const PostPage: NextPage<IProps> = ({ success, message }) => {
  return (
    <>
      <h1>{success ? message : "no data found"}</h1>
    </>
  );
};

export default PostPage;

export const getStaticProps = async () => {
  try {
    const resp = await fetch(`http://api:3030/users/1234546`);
    const body = await resp.json();
    return {
      props: body,
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        sucess: false,
        message: e,
      },
    };
  }
};
