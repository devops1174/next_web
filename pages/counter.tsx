import { NextPage } from "next";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { increment } from "../app/slices/counter";
import { AppDispatch, RootState } from "../app/store";

const CounterPage: NextPage = () => {
  const value = useSelector((state: RootState) => state.counter.value);
  const dispatch = useDispatch<AppDispatch>();
  return (
    <>
      <h1>{value}</h1>
      <button onClick={() => dispatch(increment(2))}>Increment</button>
    </>
  );
};

export default CounterPage;
