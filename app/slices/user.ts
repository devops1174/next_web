import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { httpApi } from "../http-api";

interface UserBaseModel {
  name: string;
  password: string;
}

interface UserModel extends UserBaseModel {
  email: string;
}

interface UpdateUserModel extends UserBaseModel {
  id: number;
}

export interface UserListModel extends UserModel, UpdateUserModel {}

interface StateModel {
  isLoading: boolean;
  isError: boolean;
  error: string;
  users: UserListModel[];
}

const initialState: StateModel = {
  isLoading: false,
  isError: false,
  error: "",
  users: [],
};

const createUser = createAsyncThunk("user/create", async (user: UserModel) => {
  try {
    const resp = await httpApi.post("/user", user);
    if (resp.status == 201) {
      return resp.json();
    } else {
      throw new Error("Can not create new user");
    }
  } catch (e) {
    throw new Error("Create user exception");
  }
});

const listUser = createAsyncThunk("user/list", async () => {
  try {
    const resp = await httpApi.get("/user");
    if (resp.status == 200) {
      return resp.json();
    } else {
      throw new Error("Can not get all user");
    }
  } catch (e) {
    throw new Error("Get all user exception");
  }
});

const deleteUser = createAsyncThunk("user/delete", async (id: number) => {
  try {
    const resp = await httpApi.delete(`/user/${id}`);
    if (resp.status == 200) {
      return resp.json();
    } else {
      throw new Error("Can not delete user");
    }
  } catch (e) {
    throw new Error("Delete user exception");
  }
});

const updateUser = createAsyncThunk(
  "user/update",
  async (user: UpdateUserModel) => {
    try {
      const resp = await httpApi.put(`/user/${user.id}`, user);
      if (resp.status == 200) {
        return resp.json();
      } else {
        throw new Error("Can not update user");
      }
    } catch (e) {
      throw new Error("Update user exception");
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(createUser.fulfilled, (state, action) => {})
      .addCase(deleteUser.fulfilled, (state, action) => {})
      .addCase(updateUser.fulfilled, (state, action) => {})
      .addCase(listUser.fulfilled, (state, action) => {
        state.users = action.payload.data;
      })
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/rejected");
        },
        (state, action) => {
          state.isLoading = false;
          state.isError = true;
          state.error = action.error.message;
        }
      )
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/pending");
        },
        (state, action) => {
          state.isLoading = true;
          state.isError = false;
          state.error = "";
        }
      )
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/fulfilled");
        },
        (state, action) => {
          state.isLoading = false;
        }
      );
  },
});

export default userSlice;
export { createUser, deleteUser, updateUser, listUser };
export const userReducer = userSlice.reducer;
