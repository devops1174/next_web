import { createSlice } from "@reduxjs/toolkit";

const counterSlice = createSlice({
  name: "counter",
  initialState: { value: 0 },
  reducers: {
    increment: (state, action) => {
      state.value = state.value + action.payload;
    },
  },
});

export { counterSlice };
export const { increment } = counterSlice.actions;
export default counterSlice.reducer;
