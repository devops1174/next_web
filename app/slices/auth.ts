import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { httpApi } from "../http-api";

interface UserLogin {
  email: string;
  password: string;
}

export interface AuthStateModel {
  isProcessing: boolean;
  isError: boolean;
  error: string;
  isAuth: boolean;
  token: string;
}

interface LoginRespModel {
  success: boolean;
  token: string;
  email: string;
  name: string;
}

const initialState: AuthStateModel = {
  isProcessing: false,
  isError: false,
  error: "",
  isAuth: false,
  token: "",
};

const login = createAsyncThunk("auth/login", async (userLogin: UserLogin) => {
  try {
    const resp = await httpApi.post("/login", userLogin);
    if (resp.status === 200) {
      return resp.json();
    } else {
      throw new Error("Login fail");
    }
  } catch (e) {
    throw new Error("Login fail!!!");
  }
});

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state, action) => {
        state.isProcessing = true;
      })
      .addCase(
        login.fulfilled,
        (state, action: PayloadAction<LoginRespModel>) => {
          state.isProcessing = false;
          state.isAuth = true;
          state.token = action.payload.token;
          httpApi.token = action.payload.token;
        }
      )
      .addCase(login.rejected, (state, action) => {
        state.isProcessing = false;
        state.isError = true;
        state.error = action.error.message ?? "";
      });
  },
});

export default authSlice;
export { login };
export const authReducer = authSlice.reducer;
