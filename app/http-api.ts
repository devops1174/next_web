class HttpAPI {
  host = process.env.API_HOST ?? "http://localhost:3030";
  token = "";

  async get(url: string) {
    return fetch(`${this.host}${url}`, {
      method: "GET",
      headers: this.getReqHeader(),
    });
  }

  async post(url: string, body: any) {
    return fetch(`${this.host}${url}`, {
      method: "POST",
      headers: this.getReqHeader(),
      body: JSON.stringify(body),
    });
  }

  async put(url: string, body: any) {
    return fetch(`${this.host}${url}`, {
      method: "PUT",
      headers: this.getReqHeader(),
      body: JSON.stringify(body),
    });
  }

  async delete(url: string) {
    return fetch(`${this.host}${url}`, {
      method: "DELETE",
      headers: this.getReqHeader(),
    });
  }

  getReqHeader() {
    let headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${this.token}`,
    };
    return headers;
  }
}

const httpApi = new HttpAPI();
export { httpApi };
