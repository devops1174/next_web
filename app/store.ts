import { configureStore } from "@reduxjs/toolkit";
import { authReducer } from "./slices/auth";
import counterReducer from "./slices/counter";
import { userReducer } from "./slices/user";
//
import { createWrapper, Context, HYDRATE } from "next-redux-wrapper";

//
const store = configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
    user: userReducer,
  },
});

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

const makeStore = () => store;
export type AppStore = ReturnType<typeof makeStore>;
export const wrapper = createWrapper<AppStore>(makeStore);
